from qehgatedielectric.gdcalc import GateDielectricCalculator
from qehgatedielectric.materials import make_heterostructure, \
    get_uid_list, nums2uids, load_bbs
from pathlib import Path
from numpy.random import randint

datapath = Path('/home/niflheim/jakoks/QEH/Materials/gdbbs')
nbbs = 295
max_nlayers = 10
its = 20
uid_list = get_uid_list(datapath)
bb_dict = load_bbs(uid_list, datapath)

for i in range(its):
    nlayers = randint(low=1, high=11)
    structure = randint(low=0, high=nbbs, size=nlayers)
    uids = nums2uids(structure, datapath)
    hs = make_heterostructure(uids, bb_dict)

    gd_calc = GateDielectricCalculator(hs, verbose=True)

    gd_calc.calculate()