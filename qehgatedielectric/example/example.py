from qehgatedielectric.gdcalc import GateDielectricCalculator
from qehgatedielectric.materials import make_heterostructure, \
    get_uid_list, nums2uids, load_bbs
from pathlib import Path

datapath = Path('/home/niflheim/jakoks/QEH/Materials/gdbbs')

uid_list = get_uid_list(datapath)

structure = [1, 1, 1, 3, 5, 0, 17]
uids = nums2uids(structure, datapath)
bb_dict = load_bbs(uids, datapath)
hs = make_heterostructure(uids, bb_dict=bb_dict)

gd_calc = GateDielectricCalculator(hs, verbose=True)

gd_calc.calculate()
