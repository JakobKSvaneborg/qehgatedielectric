import numpy as np
from qeh import QEH
from ase.units import Bohr, Hartree
from scipy.constants import eV, hbar
from scipy.interpolate import CubicSpline
from pathlib import Path
import matplotlib.pyplot as plt
Hz = hbar / eV  # value of 1 Hz in eV when hbar=1.
Ampere = Hz / eV
Watt = Hz / eV
cm = 1e8
MoS2_width = 6.1511


def cost(P, I):
    """
    Cost function to reinforcement learning algorithm.
    The goal is to minimize the total power loss (P) while simultaneously
    requiring that the leakage current is sufficiently small.
    For the latter, the requirements are the following:
    If I < 1e-7 A / cm^2, then the leakage is acceptable
    If I > 1e-2 A / cm^2, the leakage is completely unacceptable
    These figures are obtained from
    Xu, Yongshan, et al,  Nature Materials 22.9 (2023): 1078-1084,
    Supp. Mat, fig. 14d
    """
    Ilow = np.log(1e-7 * Ampere / cm**2)
    Ihigh = np.log(1e-2 * Ampere / cm**2)
    logI = np.log(I)

    # rescale such that I=Ilow -> x = -1, and I=Ihigh -> x=1
    x = 1 + 2 / (Ihigh - Ilow) * (logI - Ihigh)
    cost = P / (Watt / cm**2) + np.exp(10 * x)
    return cost


class GateDielectricCalculator:
    def __init__(self, heterostructure: list, kbT=0.026, mu_c=-4.308 - 0.116,
                 L0=100, eps0r=3, V0=1, frequency=1e8, verbose=False):

        self.heterostructure = heterostructure
        self.qeh = self.setup_qeh()

        self.kbT = kbT  # 26 meV ~ room temperature
        # location of channel Fermi level w.r.t vacuum level in the on-state
        self.mu_c = mu_c
        self.frequency = frequency * Hz  # switching frequency

        self.verbose = verbose

    def calculate(self):

        epsM = self.get_epsilon()
        L = self.qeh.hs.layerwidth_l.sum() * Bohr
        bias_L = self.get_gate_bias(epsM, L)
        V = bias_L - self.mu_c
        metallic = self.is_metallic()
        I = self.get_current(bias_L=bias_L, bias_R=self.mu_c)
        P_ohm = V * I
        eps0 = 1 / (4 * np.pi)
        C = eps0 * epsM / L  # unit?
        P_switch = V**2 * C / 2 * self.frequency
        P = P_ohm + P_switch
        if self.verbose:
            print(
                'uids:', [Path(gdbb.path).name
                          for gdbb in self.heterostructure])
            print(f'current: {I / (Ampere / cm**2):.4g} A/cm^2')
            print(f'bias: {bias_L - self.mu_c:.3f} eV')
            print(f'total width: {L:.2f} Å')
            print(f'frequency: {self.frequency / (1e9 * Hz):.3f} GHz')
            print(f'epsilon: {epsM:.2f}')
            print(f'EOT: {3.9 * L / epsM:.3f} Å')
            print('Metallic: ', metallic)
            print(f'Ohmic loss: {P_ohm / (Watt / cm**2):.4g} W/cm^2')
            print(f'Switching loss: {P_switch / (Watt / cm**2):.4g} W/cm^2')
            print(f'Total loss: {P / (Watt / cm**2):.4g} W/cm^2')
            print(f'Cost function: {cost(P,I):.4g}')

        return cost(P, I)

    def setup_qeh(self):
        structure = [GDbb.path for GDbb in self.heterostructure]
        layerwidth_l = [GDbb.thickness / Bohr for GDbb in self.heterostructure]
        qeh = QEH.heterostructure(structure, layerwidth_l=layerwidth_l)
        return qeh

    def get_epsilon(self):
        L = self.qeh.hs.layerwidth_l.sum() + MoS2_width / Bohr / 2
        _, _, epsM_w = self.qeh.get_macroscopic_dielectric_function(
            direction='z',
            static=True,
            width=L)
        return epsM_w[0].real

    def get_transmission(self, e_e, offsets_l=None):
        # determined from fit to hbn experiments
        # see https://doi.org/10.1038/s41928-020-00529-x, fig 2
        exp_prefactor = 1e4 / 1964  # determined from monolayer exp
        V_coupling = (3e1 / 2.735e-11)**(1 / 4)  # determined from 3layer exp
        layerwidth_l = self.qeh.hs.layerwidth_l * Bohr
        nE = len(e_e)  # num of energy points
        G0N_e = np.ones(nE, dtype=complex)
        if offsets_l is None:
            offsets_l = self.get_energy_offsets()
        for l, gdbb in enumerate(self.heterostructure):
            z_z = gdbb.zGF_z
            # widthm, width, widthp=widths of previous, current and next layers
            width = layerwidth_l[l]
            if l > 0:
                widthm = layerwidth_l[l - 1]
            else:
                widthm = 0
            if l < len(layerwidth_l) - 1:
                widthp = layerwidth_l[l + 1]
            else:
                widthp = 0
            # find indices of z that are exactly
            # between layers (i-1, i) and (i, i+1)
            dm = width / 2 + widthm / 2  # dist to prev layer
            dp = width / 2 + widthp / 2  # dist to next layer
            zm = z_z.mean() - dm / 2
            zp = z_z.mean() + dp / 2

            izm = np.argmin(np.abs(z_z - zm))
            izp = np.argmin(np.abs(z_z - zp))
            G_E = gdbb.GF_Ezz[:, izm, izp]
            E_E = gdbb.EGF_E
            spl = CubicSpline(E_E, G_E)
            G_e = spl(e_e - offsets_l[l])
            G0N_e *= G_e
        G0N_e *= V_coupling**(len(layerwidth_l) - 1)

        T_e = np.abs(G0N_e)**2 * exp_prefactor
        return T_e

    def get_energy_offsets(self, return_all=False):
        """Makes sure that vacuum levels are aligned across the entire
        heterostructure. The right vacuum level of any layer must match the
        left vacuum level of the following layer.

        Returns: offsets that must be applied to the vacuum level of each layer
        to line them all up. By definition, the 1st layer will not be shifted.
        """
        # shift over layer l
        dE_l = np.zeros(len(self.heterostructure))
        # vacuum level on left side of layer l
        evacL_l = np.zeros(len(self.heterostructure))
        thickness_l = np.zeros(len(self.heterostructure))
        for l, gdbb in enumerate(self.heterostructure):
            evacL = gdbb.evac1
            evacR = gdbb.evac2
            evacL_l[l] = evacL
            dE_l[l] = evacR - evacL
            thickness_l[l] = gdbb.thickness

        total_shift = np.sum(dE_l)
        L = thickness_l.sum()
        dipole_field = -total_shift / L

        dE_l += dipole_field * thickness_l
        # accumulated shifts from all *previous* layers
        shift_l = np.zeros(len(self.heterostructure))
        shift_l[1:] = np.cumsum(dE_l)[:-1]

        # offsets; includes accumulated shift and difference in evac
        offsets_l = evacL_l + shift_l
        if return_all:
            return offsets_l, shift_l, dipole_field
        return offsets_l

    def is_metallic(self, offsets_l=None):
        Evbm = -np.inf
        Ecbm = np.inf
        if offsets_l is None:
            offsets_l = self.get_energy_offsets()
        for l, gdbb in enumerate(self.heterostructure):
            offset = offsets_l[l]
            Evbm = max(Evbm, gdbb.Evbm + offset)
            Ecbm = min(Ecbm, gdbb.Ecbm + offset)
        if Ecbm < Evbm:
            return True
        return False

    def fermi(self, E, mu):
        return 1 / (1 + np.exp((E - mu) / self.kbT))

    def get_current(self, bias_L, bias_R, offsets_l=None):
        minbias = min(bias_L, bias_R) - 2 * self.kbT
        maxbias = max(bias_L, bias_R) + 2 * self.kbT
        # integrate over bias window plus an extra range given by kbT
        biaswindow = maxbias - minbias
        Efine_e = np.linspace(minbias, minbias + biaswindow, 200)
        T_e = self.get_transmission(e_e=Efine_e, offsets_l=offsets_l)
        dx = (Efine_e[-1] - Efine_e[0]) / (len(Efine_e) - 1)
        fermi_e = self.fermi(Efine_e, bias_L) - self.fermi(Efine_e, bias_R)
        current = np.sum(T_e * fermi_e) * dx
        return current

    def get_gate_bias(self, eps=None, L=None):
        if eps is None:
            eps = self.get_epsilon()
        if L is None:
            L = self.qeh.hs.layerwidth_l.sum() * Bohr  # length in Bohr
        n_on = 3.02e-6  # on-state charge density in channel, a.u.
        Vgc = n_on * 4 * np.pi * (L / Bohr) / eps * Hartree
        return Vgc + self.mu_c

    def plot_energy_landscape(self, ax=None, filename=None,
                              offsets_l=None, shifts_l=None, bias_window=None):
        l_l = np.arange(len(self.heterostructure))
        if offsets_l is None:
            offsets_l, shifts_l, dipole_field = self.get_energy_offsets(
                return_all=True)

        if ax is None:
            figwidth = max(8, 2 * len(l_l))
            fig, ax = plt.subplots(figsize=(figwidth, 8))
        cbm_min = np.inf
        vbm_max = -np.inf
        ones = np.ones(2)
        for l in l_l:
            offset = offsets_l[l]
            mat = self.heterostructure[l]
            Ecbm = mat.Ecbm + offset
            Evbm = mat.Evbm + offset
            cbm_min = min(cbm_min, Ecbm)
            vbm_max = max(vbm_max, Evbm)
            x = np.array([l - 0.25, l + 0.25]) + 1
            if l == 0:
                ax.plot(x, Ecbm * ones, color='r', label='CBM')
                ax.plot(x, Evbm * ones, color='b', label='VBM')
                ax.fill_between(x, Evbm * ones, Ecbm * ones,
                                alpha=0.25, label='Gap', color='gray')
            else:
                ax.plot(x, Evbm * ones, color='b')
                ax.plot(x, Ecbm * ones, color='r')
                ax.fill_between(x, Evbm * ones, Ecbm * ones,
                                alpha=0.25, color='gray')
            ax.text(x=l + 1, y=Ecbm + 0.15, s=mat.name,
                    horizontalalignment='center')
        if shifts_l is not None:
            if True:
                ax.plot(1 + l_l, shifts_l, color='gray', ls='--',
                        label='Vacuum level')
            else:
                x = np.zeros(2 * len(l_l))
                shift = np.zeros(len(x))
                x[::2] = 1 + l_l - 0.25
                x[1::2] = 1 + l_l + 0.25
                shift[::2] = shifts_l
                shift[1:-1:2] = shifts_l[1:] - 0.5 * dipole_field
                shift[-1] = 0
                ax.plot(x, shift, color='gray', ls='--',
                        label='Vacuum level')
        x = np.array([l_l[0] - 0.25, l_l[-1] + 0.25]) + 1
        y1 = ones * vbm_max
        y2 = ones * cbm_min

        if not self.is_metallic(offsets_l=offsets_l):
            ax.fill_between(x, y1, y2, alpha=0.5, color='gray')
        if bias_window is None:
            biasR = self.mu_c
            biasL = self.get_gate_bias()
            bias_window = [biasL, biasR]
        y1 = ones * min(bias_window)
        y2 = ones * max(bias_window)
        ax.fill_between(x, y1, y2, alpha=0.25,
                        label='Bias window', color='red')
        ax.set_xticks(1 + l_l)
        ax.set_xlabel('Layer number')
        ax.set_ylabel(' $E$ / eV')
        ax.legend(loc='upper right')
        if filename is not None:
            plt.savefig(filename)
        return ax
