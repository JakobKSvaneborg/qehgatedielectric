import numpy as np


class GDBuildingBlock:

    def __init__(self, uid, datapath):
        self.path = f'{datapath}/{uid}.npz'
        self.name = uid.split('-')[0]
        data = np.load(self.path)
        self.GF_Ezz = data['GF_Ezz']
        self.zGF_z = data['zGF_z']
        self.EGF_E = data['EGF_E']
        self.thickness = data['thickness']
        self.evac1 = data['evac1']
        self.evac2 = data['evac2']
        self.Evbm = data['Evbm']
        self.Ecbm = data['Ecbm']


def get_uid_list(datapath):
    uid_list = []
    with open(f'{datapath}/uids.txt', 'r') as file:
        lines = file.readlines()
    for line in lines:
        uid_list.append(line.strip())
    return uid_list


def nums2uids(numbers: list, datapath):
    uid_list = get_uid_list(datapath)
    uids = []
    for number in numbers:
        uids.append(uid_list[number])
    return uids


def load_bbs(uids, datapath):
    bb_dict = {}
    for uid in uids:
        bb = GDBuildingBlock(uid, datapath)
        bb_dict[uid] = bb
    return bb_dict


def make_heterostructure(uids, bb_dict=None, datapath=None):
    if bb_dict is None:
        assert datapath is not None
        bb_dict = load_bbs(uids, datapath)
    hs = [bb_dict[uid] for uid in uids]
    return hs
