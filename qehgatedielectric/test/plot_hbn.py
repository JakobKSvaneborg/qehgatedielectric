from qehgatedielectric.gdcalc import GateDielectricCalculator
from qehgatedielectric.materials import make_heterostructure
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import eV, hbar as hbarSI
Hz = hbarSI / eV  # value of 1 Hz in 1/eV when hbar=1.

hbn = 'BN-4a5edc763604'

max_n_layers = 4
biasR = -4.308
biasL_i = np.linspace(0, 1, 50) + biasR
I_ni = np.zeros((max_n_layers, len(biasL_i)))
plt.figure()
for n in range(max_n_layers):
    hs = make_heterostructure([hbn] * (n + 1), datapath=Path('.'))
    gdcalc = GateDielectricCalculator(hs, verbose=True)
    for i, biasL in enumerate(biasL_i):
        I_ni[n, i] = gdcalc.get_current(bias_L=biasL, bias_R=biasR)
    plt.semilogy(biasL_i - biasR, I_ni[n] / Hz * eV * 1e16, label=f'{n} layers')
    exp_data = np.genfromtxt(f'hbn{n + 1}-exp.csv', delimiter=',')
    x = exp_data[:, 0]
    y = 10**exp_data[:, 1]
    plt.semilogy(x, y, color='k')
plt.xlabel(r'$V_G /$ V')
plt.ylabel(r'$J_G /$ A/cm$^2$')
plt.legend()
plt.savefig('hbn_IV.png')
plt.show()

