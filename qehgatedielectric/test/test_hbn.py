from qehgatedielectric.gdcalc import GateDielectricCalculator
from qehgatedielectric.materials import make_heterostructure
from pathlib import Path
hbn = 'BN-4a5edc763604'

max_n_layers = 4

for n in range(max_n_layers):
    hs = make_heterostructure([hbn] * (n + 1), datapath=Path('.'))
    gdcalc = GateDielectricCalculator(hs, verbose=True)
    gdcalc.calculate()